/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package kubectl

import(
  "fmt"
  "strings"
  "os/exec"

  "github.com/spf13/cobra"
  "github.com/getsentry/raven-go"
)



func Execute(cmd *cobra.Command, args []string){

  var finalArgs = Argumentizer(cmd, args...)

  if config.Debug {
    fmt.Printf("Executing: kubectl %s\n", finalArgs)
  }

  out, err := exec.Command("kubectl", finalArgs...).CombinedOutput()
  if err != nil {
    raven.CaptureErrorAndWait(err, map[string]string{"foo": "bar"})
    fmt.Printf("Error: %s\n", err.Error())
    fmt.Printf("Error: %#v\n", err)
    fmt.Printf("output: %s\n", out)
  } else {
    if strings.Contains(string(out), "No resources found.") {
      fmt.Printf("No resources found in namespace %s\n", "")
    } else {
      fmt.Println(string(out))
    }
  }
}

func CaptureOutput(cmd *cobra.Command, args []string) string {
  var finalArgs = Argumentizer(cmd, args...)

  if config.Debug {
    fmt.Printf("Executing: kubectl %s\n", finalArgs)
  }

  output, err := exec.Command("kubectl", finalArgs...).Output()
  if err != nil {
    raven.CaptureErrorAndWait(err, map[string]string{"foo": "bar"})
    fmt.Printf("Error: %s\n", err.Error())
    fmt.Printf("Error: %#v\n", err)
    fmt.Printf("output: %s\n", output)
  }

  return string(output)
}

func GetCurrentContext() string {
  config := &cobra.Command{Use: "config"}
  return strings.TrimSpace(CaptureOutput(config, []string{"current-context"}))
}

func GetNamespace(context string) string {
  /* if no context is specified, then use current context */
  if context == "" {
    context = GetCurrentContext()
  }

  jsonpath := fmt.Sprintf("-o=jsonpath='{.contexts[?(@.name==\"%s\")].context.namespace}'", context)
  config := &cobra.Command{Use: "config"}
  namespace := strings.TrimSpace(CaptureOutput(config, []string{"view", jsonpath}))
  return strings.TrimFunc(namespace, func(r rune) bool {return r == '\''})
}

func SetNamespace(context string, namespace string) {
  config := &cobra.Command{Use: "config"}
  Execute(config, []string{"set-context", context, fmt.Sprintf("--namespace=%s", namespace)})
}
