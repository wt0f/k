/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package kubectl

import (
  //"strings"
  "fmt"

  "gitlab.com/wt0f/k/configuration"
  "github.com/spf13/cobra"
)

var config = configuration.GetConfig()

type SubcmdAttribs struct {
  Namespaced, ShowLabels, OutputWide, OutputJSON, OutputYAML bool
}

var feature = map[string]SubcmdAttribs{
  // command     : namespaced, labels, wide, json, yaml
  "get"         : {true, true, true, true, true},
  "describe"    : {true, true, false, false, false},
  "create"      : {true, false, false, false, false},
  "apply"       : {true, false, false, false, false},
  "patch"       : {true, false, false, false, false},
  "run"         : {true, false, false, false, false},
  "edit"        : {true, false, false, false, false},
  "delete"      : {true, false, false, false, false},
  "scale"       : {true, false, false, true, true},
  "cordon"      : {false, false, false, false, false},
  "uncordon"    : {false, false, false, false, false},
  "drain"       : {false, false, false, false, false},
  "taint"       : {false, false, false, true, true},
  "logs"        : {true, false, false, false, false},
  "exec"        : {true, false, false, false, false},
  "label"       : {true, false, false, true, true},
  "annotate"    : {true, false, false, true, true},
  "api-versions": {false, false, false, false, false},
  "version"     : {false, false, false, true, true},
  "help"        : {false, false, false, false, false},
  "config"      : {false, false, false , false, false},
}


func Argumentizer(cmd *cobra.Command, args ...string) []string {
  var parts []string
  subcmd := cmd.Use

  if config.Debug {
    fmt.Printf("Argumentizer input: %s %s\n", subcmd, args)
  }

  // Start by adding the subcmd to the list of arguments
  parts = append(parts, subcmd)
  // Can not do an append of varargs of varargs
  parts = append(parts, args...)

  // Add either --all-namespaces or -n NAMESPACE if applicable
  if feature[subcmd].Namespaced {
    if config.AllNamespaces {
      parts = append(parts, "--all-namespaces")
    } else if config.Namespace != "" {
      parts = append(parts, "-n", config.Namespace)
    }
  }

  // Show labels if not outputing YAML or JSON
  if feature[subcmd].ShowLabels && !(config.OutputYAML || config.OutputJSON) && config.ShowLabels {
    parts = append(parts, "--show-labels")
  }

  // Allow YAML and JSON outputs to override wide mode
  if feature[subcmd].OutputJSON && config.OutputJSON {
    parts = append(parts, "-o", "json")
  } else if feature[subcmd].OutputYAML && config.OutputYAML {
    parts = append(parts, "-o", "yaml")
  } else if feature[subcmd].OutputWide && config.OutputWide {
    parts = append(parts, "-o", "wide")
  }

  if config.Debug {
    fmt.Printf("Argumentizer output: %s\n", parts)
  }
  return parts
}
