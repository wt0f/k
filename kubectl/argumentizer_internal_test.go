/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package kubectl

import (
  "testing"
  "strings"

  "gitlab.com/wt0f/k/configuration"
  "github.com/spf13/cobra"
)

func TestNamespace(t *testing.T) {
  conf := configuration.GetConfig()
  conf.Namespace = "foo"
  conf.AllNamespaces = false

  getCmd := &cobra.Command{ Use  : "get" }
  cmd := strings.Join(Argumentizer(getCmd, "pods"), " ")
  if ! strings.Contains(cmd, "-n foo") {
    t.Error("Argumentizer does not correctly insert namespace switch")
  }
}

func TestAllNamespaces(t *testing.T) {
  conf := configuration.GetConfig()
  conf.Namespace = "foo"
  conf.AllNamespaces = true

  getCmd := &cobra.Command{ Use  : "get" }
  cmd := strings.Join(Argumentizer(getCmd, "pods"), " ")
  if strings.Contains(cmd, "-n foo") {
    t.Error("Argumentizer inserted namespace switch when searching all namespaces")
  }
  if ! strings.Contains(cmd, "--all-namespaces") {
    t.Error("Argumentizer did not insert --all-namespaces")
  }
}

func TestShowLabels(t *testing.T) {
  conf := configuration.GetConfig()
  conf.ShowLabels = true

  getCmd := &cobra.Command{ Use  : "get" }
  cmd := strings.Join(Argumentizer(getCmd, "pods"), " ")
  if ! strings.Contains(cmd, "--show-labels") {
    t.Error("Argumentizer does not insert --show-labels")
  }
}

func TestNotShowLabelsWithYAML(t *testing.T) {
  conf := configuration.GetConfig()
  conf.ShowLabels = true
  conf.OutputYAML = true

  getCmd := &cobra.Command{ Use  : "get" }
  cmd := strings.Join(Argumentizer(getCmd, "pods"), " ")
  if strings.Contains(cmd, "--show-labels") {
    t.Error("Argumentizer inserted --show-labels when displaying YAML")
  }
  if ! strings.Contains(cmd, "-o yaml") {
    t.Error("Argumentizer does not insert -o yaml")
  }
}

func TestNotShowLabelsWithJSON(t *testing.T) {
  conf := configuration.GetConfig()
  conf.ShowLabels = true
  conf.OutputJSON = true

  getCmd := &cobra.Command{ Use  : "get" }
  cmd := strings.Join(Argumentizer(getCmd, "pods"), " ")
  if strings.Contains(cmd, "--show-labels") {
    t.Error("Argumentizer inserted --show-labels when displaying JSON")
  }
  if ! strings.Contains(cmd, "-o json") {
    t.Error("Argumentizer does not insert -o json")
  }
}