


Commands
--------
* wrap most kubectl commands
    * auto add --namespace
    * auto add --show-labels
    * auto add -o wide
* decode files in configmaps and secrets
* attach to scratch pod
* oidc authentication


kubectl subcommands
-------------------
* get / show
* describe
* create
* apply
* patch
* run
* edit
* delete
* scale
* cordon / uncordon
* drain
* taint
* logs
* exec
* label
* annotate
* api-versions
* version
* help

k wrapper commands
------------------
* ns
* auth (kubeauth)
* config?
* kubebox


enhancements
------------
* colorized log output
* colorized command output

