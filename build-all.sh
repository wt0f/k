BIN_FILE_NAME_PREFIX=k
PROJECT_DIR=$2
PLATFORMS=(darwin/amd64 linux/arm64 linux/amd64 linux/arm windows/amd64 windows/386)

# Gather info about this build
version=$(git describe --always --tags | gawk 'gsub(/-.*$/, "")')
echo "version=$version"
gitref=$(git describe --always --tags | gawk 'gsub(/^.*-/, "")')
echo "gitref=$gitref"
if [ -z "$CI_COMMIT_REF_NAME" ]; then
  branch=$(git branch | grep \* | cut -d ' ' -f2)
else
  branch=$(echo -n ${CI_COMMIT_REF_NAME} | tr -cs '[0-9A-Za-z-]' '-')
fi
echo "branch=$branch"

# Setup for versions to build
publish_versions=($version)
# Check to see if this is a full release or just a development release
if [ "$branch" != 'master' ]; then 
    publish_versions=(${version}-${branch}.${gitref}  ${version}-${branch})
fi
echo "publish_versions=${publish_versions[*]}"

for PLATFORM in ${PLATFORMS[*]}; do
    # Build the executable for a platform
    GOOS=${PLATFORM%/*}
    GOARCH=${PLATFORM#*/}
    artifact_base="$PROJECT_DIR/artifacts/${GOOS}-${GOARCH}"

    mkdir -p $artifact_base
    exec_file=$BIN_FILE_NAME_PREFIX
    if [[ "${GOOS}" == "windows" ]]; then exec_file="${BIN_FILE_NAME_PREFIX}.exe"; fi

    CMD="GOOS=${GOOS} GOARCH=${GOARCH} go build -o $artifact_base/$exec_file"
    #echo $CMD
    echo "${CMD}"
    eval $CMD || FAILURES="${FAILURES} ${PLATFORM}"

    # publish artifact to nexus
    for artifact in ${publish_versions[*]}; do
        if [ -r $artifact_base/$exec_file ]; then
            curl --fail -u $NEXUS_USERNAME:$NEXUS_PASSWORD \
                --upload-file $artifact_base/$exec_file \
                "https://nexus.wt0f.com/repository/k/$artifact/${GOOS}-${GOARCH}/$exec_file"
        fi
    done

done
