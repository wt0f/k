/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "fmt"

  "gitlab.com/wt0f/k/kubectl"

  "github.com/spf13/cobra"
)


func init() {
  rootCmd.AddCommand(namespaceCmd)
}

var namespaceCmd = &cobra.Command{
  Use  : "ns",
  Short: "Set or view the current namespace",
  Long : "Allow the namespace to be set for all executions of k",
  Run  : func(cmd *cobra.Command, args []string) {
    executeNs(args)
  },
}

func executeNs(args []string){

  // Get the current context and namespace from kubectl
  context := kubectl.GetCurrentContext()
  namespace := kubectl.GetNamespace(context)

  if (len(args) > 0) {
    if args[0] != "-" {
      // Setting the namespace
      kubectl.SetNamespace(context, args[0])
    } else {
      // Removing the namespace
      kubectl.SetNamespace(context, "")
    }
  } else {
    fmt.Printf("%s\n", namespace)
  }
}