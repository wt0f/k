/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "gitlab.com/wt0f/k/kubectl"
  "github.com/spf13/cobra"
)

var (
  includeUninitialized  bool
  showEvents            bool
)

const (
  includeUninitializedHelp = "If true, the kubectl command applies to uninitialized objects. If explicitly set to false, this flag overrides other flags that make the kubectl commands apply to uninitialized objects, e.g., '--all'. Objects with empty metadata.initializers are regarded as initialized."
  showEventsHelp = "If true, display events related to the described object."
)
func init() {
  rootCmd.AddCommand(describeCmd)
  describeCmd.Flags().BoolVar(&includeUninitialized, "include-uninitialized", false, includeUninitializedHelp)
  describeCmd.Flags().BoolVar(&showEvents, "show-events", true, showEventsHelp)
}

var describeCmd = &cobra.Command{
  Use  : "describe",
  Short: "kubectl describe ...",
  Long : "Wrap kubectl describe with namespace options",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addSwitch(&combinedArgs, "--include-uninitialized", includeUninitialized)
    addSwitch(&combinedArgs, "--show-events", showEvents)
    kubectl.Execute(cmd, combinedArgs)
  },
}
