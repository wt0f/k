/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "gitlab.com/wt0f/k/kubectl"
  "github.com/spf13/cobra"
)

var (
  deleteAll             bool
  deleteCascade         bool
  deleteFilename        string
  deleteForce           bool
  deleteGracePeriod     string
  deleteIgnoreNotFound  bool
  deleteNow             bool
  deleteSelector        string
  deleteTimeout         string
  deleteWait            bool
)

const (
  deleteAllHelp            = "Delete all resources, including uninitialized ones, in the namespace of the specified resource types."
  deleteCascadeHelp        = "If true, cascade the deletion of the resources managed by this resource (e.g. Pods created by a ReplicationController).  Default true."
  deleteFilenameHelp       = "containing the resource to delete."
  deleteForceHelp          = "Only used when grace-period=0. If true, immediately remove resources from API and bypass graceful deletion. Note that immediate deletion of some resources may result in inconsistency or data loss and requires confirmation."
  deleteGracePeriodHelp    = "Period of time in seconds given to the resource to terminate gracefully. Ignored if negative. Set to 1 for immediate shutdown. Can only be set to 0 when --force is true (force deletion)."
  deleteIgnoreNotFoundHelp = "Treat 'resource not found' as a successful delete. Defaults to 'true' when --all is specified."
  deleteSelectorHelp       = "Selector (label query) to filter on, not including uninitialized ones."
  deleteNowHelp            = "If true, resources are signaled for immediate shutdown (same as --grace-period=1)."
  deleteTimeoutHelp        = "The length of time to wait before giving up on a delete, zero means determine a timeout from the size of the object"
  deleteWaitHelp           = "If true, wait for resources to be gone before returning. This waits for finalizers."
)

func init() {
  rootCmd.AddCommand(deleteCmd)
  deleteCmd.Flags().BoolVar(&deleteAll, "all", false, deleteAllHelp)
  deleteCmd.Flags().BoolVar(&deleteCascade, "cascade", true, deleteCascadeHelp)
  deleteCmd.Flags().StringVarP(&deleteFilename, "filename", "f", "", deleteFilenameHelp)
  deleteCmd.Flags().BoolVar(&deleteForce, "force", false, deleteForceHelp)
  deleteCmd.Flags().StringVar(&deleteGracePeriod, "grace-period", "-1", deleteGracePeriodHelp)
  deleteCmd.Flags().BoolVar(&deleteIgnoreNotFound, "ignore-not-found", false, deleteIgnoreNotFoundHelp)
  deleteCmd.Flags().StringVarP(&deleteSelector, "selector", "l", "", deleteSelectorHelp)
  deleteCmd.Flags().BoolVar(&deleteNow, "now", false, deleteNowHelp)
  deleteCmd.Flags().StringVar(&deleteTimeout, "timeout", "0s", deleteTimeoutHelp)
  deleteCmd.Flags().BoolVar(&deleteWait, "wait", false, deleteWaitHelp)
}

var deleteCmd = &cobra.Command{
  Use  : "delete",
  Short: "kubectl delete ...",
  Long : "Wrap kubectl delete with namespace options",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addParam(&combinedArgs, "--filename", deleteFilename)
    addSwitch(&combinedArgs, "--all", deleteAll)
    addSwitch(&combinedArgs, "--cascade", deleteCascade)
    addParam(&combinedArgs, "--grace-period", deleteGracePeriod)
    addSwitch(&combinedArgs, "--force", deleteForce)
    addSwitch(&combinedArgs, "--ignore-not-found", deleteIgnoreNotFound)
    addSwitch(&combinedArgs, "--now", deleteNow)
    addParam(&combinedArgs, "--timeout", deleteTimeout)
    addSwitch(&combinedArgs, "--wait", deleteWait)
    addParam(&combinedArgs, "--selector", deleteSelector)
    kubectl.Execute(cmd, combinedArgs)
  },
}
