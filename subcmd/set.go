/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "fmt"

  "github.com/spf13/cobra"
)

func init() {
  rootCmd.AddCommand(setCmd)
}

var setCmd = &cobra.Command{
  Use  : "set",
  Short: "Set a config value",
  Long : "",
  Run  : func(cmd *cobra.Command, args []string) {
    executeSet(args)
  },
}

func executeSet(args []string){
  fmt.Println("You called set")
}