/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "gitlab.com/wt0f/k/kubectl"
  "github.com/spf13/cobra"
)


var (
  attach          bool
  cascade         bool
  command         bool
  runDryRun       bool
  expose          bool
  image           string
  leaveStdinOpen  bool
  limits          string
  port            string
  quiet           bool
  record          bool
  replicas        string
  requests        string
  restart         string
  rm              bool
  saveConfig      bool
  schedule        string
  serviceAccount  string
  runStdin        bool
  runTimeout      string
  runTty          bool
  runWatch        bool
)

const (
  attachHelp         = "If true, wait for the Pod to start running, and then attach to the Pod as if 'kubectl attach ...' were called.  Default false, unless '-i/--stdin' is set, in which case the default is true. With '--restart=Never' the exit code of the container process is returned."
  cascadeHelp        = "If true, cascade the deletion of the resources managed by this resource (e.g. Pods created by a ReplicationController).  Default true."
  commandHelp        = "If true and extra arguments are present, use them as the 'command' field in the container, rather than the 'args' field which is the default."
  runDryRunHelp      = "If true, only print the object that would be sent, without sending it."
  exposeHelp         = "If true, a public, external service is created for the container(s) which are run"
  imageHelp          = "The image for the container to run."
  leaveStdinOpenHelp = "If the pod is started in interactive mode or with stdin, leave stdin open after the first attach completes. By default, stdin will be closed after the first attach completes."
  limitsHelp         = "The resource requirement limits for this container.  For example, 'cpu=200m,memory=512Mi'. Note that server side components may assign limits depending on the server configuration, such as limit ranges."
  portHelp           = "The port that this container exposes.  If --expose is true, this is also the port used by the service that is created."
  quietHelp          = "If true, suppress prompt messages."
  recordHelp         = "Record current kubectl command in the resource annotation. If set to false, do not record the command. If set to true, record the command. If not set, default to updating the existing annotation value only if one already exists."
  replicasHelp       = "Number of replicas to create for this container. Default is 1."
  requestsHelp       = "The resource requirement requests for this container.  For example, 'cpu=100m,memory=256Mi'. Note that server side components may assign requests depending on the server configuration, such as limit ranges."
  restartHelp        = "The restart policy for this Pod.  Legal values [Always, OnFailure, Never].  If set to 'Always' a deployment is created, if set to 'OnFailure' a job is created, if set to 'Never', a regular pod is created. For the latter two --replicas must be 1.  Default 'Always', for CronJobs `Never`."
  rmHelp             = "If true, delete resources created in this command for attached containers."
  saveConfigHelp     = "If true, the configuration of current object will be saved in its annotation. Otherwise, the annotation will be unchanged. This flag is useful when you want to perform kubectl apply on this object in the future."
  scheduleHelp       = "A schedule in the Cron format the job should be run with."
  serviceAccountHelp = "Service account to set in the pod spec"
  runStdinHelp       = "Keep stdin open on the container(s) in the pod, even if nothing is attached."
  runTimeoutHelp     = "The length of time to wait before giving up on a delete, zero means determine a timeout from the size of the object"
  runTtyHelp         = "Allocated a TTY for each container in the pod."
  runWatchHelp       = "If true, wait for resources to be gone before returning. This waits for finalizers."
)


func init() {
  rootCmd.AddCommand(runCmd)
  runCmd.Flags().BoolVar(&attach, "attach", false, attachHelp)
  runCmd.Flags().BoolVar(&cascade, "cascade", true, cascadeHelp)
  runCmd.Flags().BoolVar(&command, "command", false, commandHelp)
  runCmd.Flags().BoolVar(&runDryRun, "dry-run", false, runDryRunHelp)
  runCmd.Flags().BoolVar(&expose, "expose", false, exposeHelp)
  runCmd.Flags().StringVar(&image, "image", "", imageHelp)
  runCmd.Flags().BoolVar(&leaveStdinOpen, "leave-stdin-open", false, leaveStdinOpenHelp)
  runCmd.Flags().StringVar(&limits, "limits", "", limitsHelp)
  runCmd.Flags().StringVar(&port, "port", "", portHelp)
  runCmd.Flags().BoolVar(&quiet, "quiet", false, quietHelp)
  runCmd.Flags().BoolVar(&record, "record", false, recordHelp)
  runCmd.Flags().StringVarP(&replicas, "replicas", "r", "", replicasHelp)
  runCmd.Flags().StringVar(&requests, "requests", "", requestsHelp)
  runCmd.Flags().StringVar(&restart, "restart", "", restartHelp)
  runCmd.Flags().BoolVar(&rm, "rm", false, rmHelp)
  runCmd.Flags().BoolVar(&saveConfig, "save-config", false, saveConfigHelp)
  runCmd.Flags().StringVar(&schedule, "schedule", "", scheduleHelp)
  runCmd.Flags().StringVar(&serviceAccount, "service-account", "", serviceAccountHelp)
  runCmd.Flags().BoolVarP(&runStdin, "stdin", "i", false, runStdinHelp)
  runCmd.Flags().StringVar(&runTimeout, "timeout", "", runTimeoutHelp)
  runCmd.Flags().BoolVarP(&runTty, "tty", "t", false, runTtyHelp)
  runCmd.Flags().BoolVar(&runWatch, "watch", false, runWatchHelp)
}

var runCmd = &cobra.Command{
  Use  : "run",
  Short: "kubectl run ...",
  Long : "Wrap kubectl run with namespace options",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addSwitch(&combinedArgs, "--attach", attach)
    addSwitch(&combinedArgs, "--cascade", cascade)
    addSwitch(&combinedArgs, "--command", command)
    addSwitch(&combinedArgs, "--dry-run", runDryRun)
    addSwitch(&combinedArgs, "--expose", expose)
    addParam(&combinedArgs, "--image", image)
    addSwitch(&combinedArgs, "--leave-stdin-open", leaveStdinOpen)
    addParam(&combinedArgs, "--limits", limits)
    addParam(&combinedArgs, "--port", port)
    addSwitch(&combinedArgs, "--quiet", quiet)
    addSwitch(&combinedArgs, "--record", record)
    addParam(&combinedArgs, "--replicas", replicas)
    addParam(&combinedArgs, "--requests", requests)
    addParam(&combinedArgs, "--restart", restart)
    addSwitch(&combinedArgs, "--rm", rm)
    addSwitch(&combinedArgs, "--save-config", saveConfig)
    addParam(&combinedArgs, "--schedule", schedule)
    addParam(&combinedArgs, "--service-account", serviceAccount)
    addSwitch(&combinedArgs, "--stdin", runStdin)
    addParam(&combinedArgs, "--timeout", runTimeout)
    addSwitch(&combinedArgs, "--tty", runTty)
    addSwitch(&combinedArgs, "--watch", runWatch)
    kubectl.Execute(cmd, combinedArgs)
  },
}
