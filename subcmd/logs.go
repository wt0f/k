/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "gitlab.com/wt0f/k/kubectl"
  "github.com/spf13/cobra"
)

var (
  allContainers     bool
  logsContainer     string
  follow            bool
  previous          bool
  logsSelector      string
  since             string
  sinceTime         string
  tail              string
  timestamps        bool
)

const (
  allContainersHelp     = "Get all containers's logs in the pod(s)."
  logsContainerHelp     = "Print the logs of this container"
  logsSelectorHelp      = "Selector (label query) to filter on."
  followHelp            = "Specify if the logs should be streamed."
  previousHelp          = "If true, print the logs for the previous instance of the container in a pod if it exists."
  sinceHelp             = "Only return logs newer than a relative duration like 5s, 2m, or 3h. Defaults to all logs. Only one of since-time / since may be used."
  sinceTimeHelp         = "Only return logs after a specific date (RFC3339). Defaults to all logs. Only one of since-time / since may be used."
  tailHelp              = "Lines of recent log file to display. Defaults to -1 with no selector, showing all log lines otherwise 10, if a selector is provided."
  timestampsHelp        = "Include timestamps on each line in the log output"
)


func init() {
  rootCmd.AddCommand(logsCmd)
  logsCmd.Flags().BoolVar(&allContainers, "all-containers", false, allContainersHelp)
  logsCmd.Flags().StringVarP(&logsContainer, "container", "c", "", logsContainerHelp)
  logsCmd.Flags().BoolVarP(&follow, "follow", "f", false, followHelp)
  logsCmd.Flags().StringVarP(&logsSelector, "selector", "l", "", logsSelectorHelp)
  logsCmd.Flags().BoolVarP(&previous, "previous", "p", false, previousHelp)
  logsCmd.Flags().StringVar(&since, "since", "", sinceHelp)
  logsCmd.Flags().StringVar(&sinceTime, "since-time", "", sinceTimeHelp)
  logsCmd.Flags().StringVar(&tail, "tail", "", tailHelp)
  logsCmd.Flags().BoolVar(&timestamps, "timestamps", false, timestampsHelp)
}

var logsCmd = &cobra.Command{
  Use  : "logs",
  Short: "kubectl logs ...",
  Long : "Wrap kubectl logs with namespace and output options",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addSwitch(&combinedArgs, "--all-containers", allContainers)
    addParam(&combinedArgs, "--container", logsContainer)
    addSwitch(&combinedArgs, "--follow", follow)
    addParam(&combinedArgs, "--selector", logsSelector)
    addSwitch(&combinedArgs, "--previous", previous)
    addParam(&combinedArgs, "--since", since)
    addParam(&combinedArgs, "--since-time", sinceTime)
    addParam(&combinedArgs, "--tail", tail)
    addSwitch(&combinedArgs, "--timestamps", timestamps)
    kubectl.Execute(cmd, combinedArgs)
  },
}
