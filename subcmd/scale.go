/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "gitlab.com/wt0f/k/kubectl"
  "github.com/spf13/cobra"
)

var (
  scaleAll                bool
  scaleCurrentReplicas    string
  scaleFilename           string
  scaleRecord             bool
  scaleReplicas           string
  scaleResourceVersion    string
  scaleTimeout            string
)

const (
  scaleAllHelp              = "Select all resources in the namespace of the specified resource types"
  scaleCurrentReplicasHelp  = "Precondition for current size. Requires that the current size of the resource match this value in order to scale."
  scaleFilenameHelp         = "Filename, directory, or URL to files identifying the resource to set a new size"
  scaleRecordHelp           = "Record current kubectl command in the resource annotation. If set to false, do not record the command. If set to true, record the command. If not set, default to updating the existing annotation value only if one already exists."
  scaleReplicasHelp         = "The new desired number of replicas. Required."
  scaleResourceVersionHelp  = "Precondition for resource version. Requires that the current resource version match this value in order to scale."
  scaleTimeoutHelp          = "The length of time to wait before giving up on a scale operation, zero means don't wait. Any other values should contain a corresponding time unit (e.g. 1s, 2m, 3h)."
)


func init() {
  rootCmd.AddCommand(scaleCmd)
  scaleCmd.Flags().BoolVar(&scaleAll, "all", false, scaleAllHelp)
  scaleCmd.Flags().StringVar(&scaleCurrentReplicas, "current-replicas", "-1", scaleCurrentReplicasHelp)
  scaleCmd.Flags().StringVarP(&scaleFilename, "filename", "f", "", scaleFilenameHelp)
  scaleCmd.Flags().BoolVar(&scaleRecord, "record", false, scaleRecordHelp)
  scaleCmd.Flags().StringVar(&scaleReplicas, "replicas", "1", scaleRecordHelp)
  scaleCmd.Flags().StringVar(&scaleResourceVersion, "resource-version", "", scaleResourceVersionHelp)
  scaleCmd.Flags().StringVar(&scaleTimeout, "timeout", "0s", scaleTimeoutHelp)
}

var scaleCmd = &cobra.Command{
  Use  : "scale",
  Short: "kubectl scale ...",
  Long : "Wrap kubectl scale with namespace and output options",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addSwitch(&combinedArgs, "--all", scaleAll)
    addParam(&combinedArgs, "--current-replicas", scaleCurrentReplicas)
    addParam(&combinedArgs, "--filename", scaleFilename)
    addSwitch(&combinedArgs, "--record", scaleRecord)
    addParam(&combinedArgs, "--replicas", scaleReplicas)
    addParam(&combinedArgs, "--resource-version", scaleResourceVersion)
    addParam(&combinedArgs, "--timeout", scaleTimeout)
    kubectl.Execute(cmd, combinedArgs)
  },
}
