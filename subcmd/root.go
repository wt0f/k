/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "os"
  "fmt"

  "github.com/spf13/cobra"
  "github.com/spf13/pflag"
)
var rootCmd = &cobra.Command{
  Use  : "k",
  Short: "Automation wrapper around kubectl",
  Long : "Kubectl replacement and automation wrapper",
  Run  : func(cmd *cobra.Command, args []string) {
    // Do Stuff Here

  },
}

func Execute() {
  if err := rootCmd.Execute(); err != nil {
    fmt.Println(err)
    os.Exit(1)
  }
}

func PersistentFlags() *pflag.FlagSet {
  return rootCmd.PersistentFlags()
}
