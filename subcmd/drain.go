/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "gitlab.com/wt0f/k/kubectl"
  "github.com/spf13/cobra"
)

var (
  deleteLocalData   bool
  drainDryRun       bool
  drainForce        bool
  gracePeriod       string
  ignoreDaemonsets  bool
  podSelector       string
  drainTimeout      string
)

const (
  deleteLocalDataHelp  = "Continue even if there are pods using emptyDir (local data that will be deleted when the node is drained)."
  drainDryRunHelp      = "If true, only print the object that would be sent, without sending it."
  drainForceHelp       = "Continue even if there are pods not managed by a ReplicationController, ReplicaSet, Job, DaemonSet or StatefulSet."
  gracePeriodHelp      = "Period of time in seconds given to each pod to terminate gracefully. If negative, the default value specified in the pod will be used."
  ignoreDaemonsetsHelp = "Ignore DaemonSet-managed pods."
  podSelectorHelp      = "Label selector to filter pods on the node"
  drainTimeoutHelp     = "The length of time to wait before giving up, zero means infinite"
)


func init() {
  rootCmd.AddCommand(drainCmd)
  drainCmd.Flags().BoolVar(&deleteLocalData, "delete-local-data", false, deleteLocalDataHelp)
  drainCmd.Flags().BoolVar(&drainDryRun, "dry-run", false, drainDryRunHelp)
  drainCmd.Flags().BoolVar(&drainForce, "force", false, drainForceHelp)
  drainCmd.Flags().StringVar(&gracePeriod, "grace-period", "-1", gracePeriodHelp)
  drainCmd.Flags().BoolVar(&ignoreDaemonsets, "ignore-daemonsets", false, ignoreDaemonsetsHelp)
  drainCmd.Flags().StringVar(&podSelector, "pod-selector", "", podSelectorHelp)
  drainCmd.Flags().StringVar(&drainTimeout, "timeout", "", drainTimeoutHelp)
}

var drainCmd = &cobra.Command{
  Use  : "drain",
  Short: "kubectl drain ...",
  Long : "Wrap kubectl drain",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addSwitch(&combinedArgs, "--delete-local-data", deleteLocalData)
    addSwitch(&combinedArgs, "--dry-run", drainDryRun)
    addSwitch(&combinedArgs, "--force", drainForce)
    addParam(&combinedArgs, "--grace-period", gracePeriod)
    addSwitch(&combinedArgs, "--ignore-daemonsets", ignoreDaemonsets)
    addParam(&combinedArgs, "--pod-selector", podSelector)
    addParam(&combinedArgs, "--timeout", drainTimeout)
    kubectl.Execute(cmd, combinedArgs)
  },
}
