/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "gitlab.com/wt0f/k/kubectl"
  "github.com/spf13/cobra"
)

var (
  miscClient    bool
  miscShort     bool
  miscVerbs     string
  miscApiGroups string
  miscCached    bool
  miscNamespaced bool
)

const (
  miscClientHelp = "Client version only (no server required)."
  miscShortHelp  = "Print just the version number."
  miscVerbsHelp  = "Limit to resources that support the specified verbs."
  miscApiGroupsHelp = "Limit to resources in the specified API group."
  miscCachedHelp = "If false, non-namespaced resources will be returned, otherwise returning namespaced resources by default."
  miscNamespacedHelp = "If false, non-namespaced resources will be returned, otherwise returning namespaced resources by default."
)

func init() {
  rootCmd.AddCommand(apiVersionsCmd)

  rootCmd.AddCommand(versionCmd)
  versionCmd.Flags().BoolVar(&miscClient, "client", false, miscClientHelp)
  versionCmd.Flags().BoolVar(&miscShort, "version", false, miscShortHelp)

  rootCmd.AddCommand(apiResourcesCmd)
  apiResourcesCmd.Flags().StringVar(&miscVerbs, "verbs", "", miscVerbsHelp)
  apiResourcesCmd.Flags().BoolVar(&miscCached, "cached", false, miscCachedHelp)
  apiResourcesCmd.Flags().StringVar(&miscApiGroups, "api-groups", "", miscApiGroupsHelp)
  apiResourcesCmd.Flags().BoolVar(&miscNamespaced, "namespaced", true, miscNamespacedHelp)
}

var apiVersionsCmd = &cobra.Command{
  Use  : "api-versions",
  Short: "kubectl api-versions ...",
  Long : "Wrap kubectl api-versions",
  Run  : func(cmd *cobra.Command, args []string) {
    kubectl.Execute(cmd, args)
  },
}

var versionCmd = &cobra.Command{
  Use  : "version",
  Short: "kubectl version",
  Long : "Wrap kubectl version",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addSwitch(&combinedArgs, "--client", miscClient)
    addSwitch(&combinedArgs, "--short", miscShort)
    kubectl.Execute(cmd, combinedArgs)
  },
}

var apiResourcesCmd = &cobra.Command{
  Use: "api-resources",
  Short: "kubectl api-resources",
  Long: "Wrap kubectl api-resources",
  Run: func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addParam(&combinedArgs, "--verbs", miscVerbs)
    addSwitch(&combinedArgs, "--cached", miscCached)
    addParam(&combinedArgs, "--api-groups", miscApiGroups)
    addSwitch(&combinedArgs, "--namespaced", miscNamespaced)
    kubectl.Execute(cmd, combinedArgs)
  },
}
