/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "gitlab.com/wt0f/k/kubectl"
  "github.com/spf13/cobra"
)

var (
  execStdin     bool
  execTty       bool
  execContainer string
)

const (
  execStdinHelp     = "Pass stdin to the container"
  execTtyHelp       = "Stdin is a TTY"
  execContainerHelp = "Container name. If omitted, the first container in the pod will be chosen"
)


func init() {
  rootCmd.AddCommand(execCmd)
  execCmd.Flags().BoolVarP(&execStdin, "stdin", "i", false, execStdinHelp)
  execCmd.Flags().BoolVarP(&execTty, "tty", "t", false, execTtyHelp)
  execCmd.Flags().StringVarP(&execContainer, "Container", "c", "", execContainerHelp)
}

var execCmd = &cobra.Command{
  Use  : "exec",
  Short: "kubectl exec ...",
  Long : "Wrap kubectl exec with namespace options",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addSwitch(&combinedArgs, "--stdin", execStdin)
    addSwitch(&combinedArgs, "--tty", execTty)
    addParam(&combinedArgs, "--container", execContainer)
    kubectl.Execute(cmd, combinedArgs)
  },
}
