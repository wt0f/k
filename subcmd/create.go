/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "gitlab.com/wt0f/k/kubectl"
  "github.com/spf13/cobra"
)

var (
  createInputFile       string
  createDryRun          bool
  createSaveConfig      bool
  createValidate        bool
  applyOverwrite        bool
  applyRecord           bool
  applyServerDryRun     bool
  applyWait             bool
  patchPatch            string
)

const (
  createInputFileHelp   = "Filename, directory, or URL to files to use to create the resource"
  createDryRunHelp      = "If true, only print the object that would be sent, without sending it."
  createSaveConfigHelp  = "If true, the configuration of current object will be saved in its annotation. Otherwise, the annotation will be unchanged. This flag is useful when you want to perform kubectl apply on this object in the future."
  createValidateHelp    = "If true, use a schema to validate the input before sending it"
  applyOverwriteHelp    = "Automatically resolve conflicts between the modified and live configuration by using values from the modified configuration"
  applyRecordHelp       = "Record current kubectl command in the resource annotation. If set to false, do not record the command. If set to true, record the command. If not set, default to updating the existing annotation value only if one already exists."
  applyServerDryRunHelp = "If true, request will be sent to server with dry-run flag, which means the modifications won't be persisted. This is an alpha feature and flag."
  applyWaitHelp         = "If true, wait for resources to be gone before returning. This waits for finalizers."
  patchPatchHelp        = "The patch to be applied to the resource JSON file."
)


func init() {
  rootCmd.AddCommand(createCmd)
  createCmd.Flags().StringVarP(&createInputFile, "filename", "f", "", createInputFileHelp)
  createCmd.Flags().BoolVar(&createDryRun, "dry-run", false, createDryRunHelp)
  createCmd.Flags().BoolVar(&createSaveConfig, "save-config", false, createSaveConfigHelp)
  createCmd.Flags().BoolVar(&createValidate, "validate", false, createValidateHelp)

  rootCmd.AddCommand(applyCmd)
  applyCmd.Flags().StringVarP(&createInputFile, "filename", "f", "", createInputFileHelp)
  applyCmd.Flags().BoolVar(&createDryRun, "dry-run", false, createDryRunHelp)
  applyCmd.Flags().BoolVar(&createValidate, "validate", false, createValidateHelp)
  applyCmd.Flags().BoolVar(&applyOverwrite, "overwrite", true, applyOverwriteHelp)
  applyCmd.Flags().BoolVar(&applyRecord, "record", false, applyRecordHelp)
  applyCmd.Flags().BoolVar(&applyServerDryRun, "server-dry-run", false, applyServerDryRunHelp)
  applyCmd.Flags().BoolVar(&applyWait, "wait", false, applyWaitHelp)

  rootCmd.AddCommand(patchCmd)
  patchCmd.Flags().StringVarP(&createInputFile, "filename", "f", "", createInputFileHelp)
  patchCmd.Flags().BoolVar(&createDryRun, "dry-run", false, createDryRunHelp)
  patchCmd.Flags().StringVar(&patchPatch, "patch", "", patchPatchHelp)
  patchCmd.Flags().StringVar(&patchPatch, "p", "", patchPatchHelp)
  patchCmd.Flags().BoolVar(&applyRecord, "record", false, applyRecordHelp)
}

var createCmd = &cobra.Command{
  Use  : "create",
  Short: "kubectl create ...",
  Long : "Wrap kubectl create with namespace options",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addParam(&combinedArgs, "--filename", createInputFile)
    addSwitch(&combinedArgs, "--dry-run", createDryRun)
    addSwitch(&combinedArgs, "--save-config", createSaveConfig)
    addSwitch(&combinedArgs, "--validate", createValidate)
    kubectl.Execute(cmd, combinedArgs)
  },
}

var applyCmd = &cobra.Command{
  Use  : "apply",
  Short: "kubectl apply ...",
  Long : "Wrap kubectl apply with namespace options",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addParam(&combinedArgs, "--filename", createInputFile)
    addSwitch(&combinedArgs, "--dry-run", createDryRun)
    addSwitch(&combinedArgs, "--record", applyRecord)
    addSwitch(&combinedArgs, "--server-dry-run", applyServerDryRun)
    addSwitch(&combinedArgs, "--wait", applyWait)
    kubectl.Execute(cmd, combinedArgs)
  },
}

var patchCmd = &cobra.Command{
  Use  : "patch",
  Short: "kubectl patch ...",
  Long : "Wrap kubectl patch with namespace options",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addParam(&combinedArgs, "--filename", createInputFile)
    addSwitch(&combinedArgs, "--dry-run", createDryRun)
    addSwitch(&combinedArgs, "--record", applyRecord)
    addParam(&combinedArgs, "--patch", patchPatch)
    kubectl.Execute(cmd, combinedArgs)
  },
}