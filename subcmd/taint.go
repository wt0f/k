/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "gitlab.com/wt0f/k/kubectl"
  "github.com/spf13/cobra"
)

var (
  taintAll       bool
  taintOverwrite bool
  taintValidate  bool
)

const (
  taintAllHelp = "Select all nodes in the cluster"
  taintOverwriteHelp = "If true, allow taints to be overwritten, otherwise reject taint updates that overwrite existing taints."
  taintValidateHelp = "If true, use a schema to validate the input before sending it"
)

func init() {
  rootCmd.AddCommand(taintCmd)
  taintCmd.Flags().BoolVar(&taintAll, "all", false, taintAllHelp)
  taintCmd.Flags().BoolVar(&taintOverwrite, "overwrite", false, taintOverwriteHelp)
  taintCmd.Flags().BoolVar(&taintValidate, "validate", true, taintValidateHelp)
}

var taintCmd = &cobra.Command{
  Use  : "taint",
  Short: "kubectl taint ...",
  Long : "Wrap kubectl taint",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addSwitch(&combinedArgs, "--all", taintAll)
    addSwitch(&combinedArgs, "--overwrite", taintOverwrite)
    addSwitch(&combinedArgs, "--validate", taintValidate)
    kubectl.Execute(cmd, combinedArgs)
  },
}
