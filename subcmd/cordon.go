/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "gitlab.com/wt0f/k/kubectl"
  "github.com/spf13/cobra"
)

var cordonDryRun bool

const cordonDryRunHelp = "If true, only print the object that would be sent, without sending it."


func init() {
  rootCmd.AddCommand(cordonCmd)
  rootCmd.AddCommand(uncordonCmd)
  cordonCmd.Flags().BoolVar(&cordonDryRun, "dry-run", false, cordonDryRunHelp)
  uncordonCmd.Flags().BoolVar(&cordonDryRun, "dry-run", false, cordonDryRunHelp)
}

var cordonCmd = &cobra.Command{
  Use  : "cordon",
  Short: "kubectl cordon ...",
  Long : "Wrap kubectl cordon",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addSwitch(&combinedArgs, "--dry-run", cordonDryRun)
    kubectl.Execute(cmd, combinedArgs)
  },
}

var uncordonCmd = &cobra.Command{
  Use  : "cordon",
  Short: "kubectl cordon ...",
  Long : "Wrap kubectl cordon",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addSwitch(&combinedArgs, "--dry-run", cordonDryRun)
    kubectl.Execute(cmd, combinedArgs)
  },
}