/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package subcmd

import (
  "gitlab.com/wt0f/k/kubectl"
  "github.com/spf13/cobra"
)

var (
  rawURI  string
  sortBy  string
  template string
  watch    bool
  watchOnly  bool
)

const (
  rawURIHelp = "Raw URI to request from the server.  Uses the transport specified by the kubeconfig file."
  sortByHelp = "If non-empty, sort list types using this field specification.  The field specification is expressed as a JSONPath expression (e.g. '{.metadata.name}'). The field in the API resource specified by this JSONPath expression must be an integer or a string."
  templateHelp = "Template string or path to template file to use when -o=go-template, -o=go-template-file. The template format is golang templates [http://golang.org/pkg/text/template/#pkg-overview]."
  watchHelp = "After listing/getting the requested object, watch for changes. Uninitialized objects are excluded if no object name is provided."
  watchOnlyHelp = "Watch for changes to the requested object(s), without listing/getting first."
)

func init() {
  rootCmd.AddCommand(getCmd)
  getCmd.Flags().StringVar(&rawURI, "raw", "", rawURIHelp)
  getCmd.Flags().StringVar(&sortBy, "sort-by", "", sortByHelp)
  getCmd.Flags().StringVar(&template, "template", "", templateHelp)
  getCmd.Flags().BoolVarP(&watch, "watch", "w", false, watchHelp)
  getCmd.Flags().BoolVar(&watchOnly, "watch-only", false, watchOnlyHelp)
}

var getCmd = &cobra.Command{
  Use  : "get",
  Aliases: []string{"show"},
  Short: "kubectl get ...",
  Long : "Wrap kubectl get with namespace and output options",
  Run  : func(cmd *cobra.Command, args []string) {
    var combinedArgs []string = args
    addParam(&combinedArgs, "--raw", rawURI)
    addParam(&combinedArgs, "--sort-by", sortBy)
    addParam(&combinedArgs, "--template", template)
    addSwitch(&combinedArgs, "--watch", watch)
    addSwitch(&combinedArgs, "--watch-only", watchOnly)
    kubectl.Execute(cmd, combinedArgs)
  },
}
