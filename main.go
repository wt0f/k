/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package main

import (
  "flag"
  "fmt"
  "os"
  "runtime"

  "gitlab.com/wt0f/k/configuration"
  "gitlab.com/wt0f/k/subcmd"
  
  "github.com/spf13/cobra"
  "github.com/getsentry/raven-go"
  "github.com/op/go-logging"
)

var prettyFormat = logging.MustStringFormatter(
  "%{color}▶ %{message} %{color:reset}",
)
var plainFormat = logging.MustStringFormatter(
  "[%{level}] - %{message}",
)

var (
  flagLevel       string = "info"
  flagShowHelp    bool
  flagShowVersion bool
  flagPrettyLog   bool
  VERSION         string = "dev"
  BUILD_DATE      string = ""
)

var log = logging.MustGetLogger("k")

const (
  verboseHelp = "verbose output"
  namespaceHelp = "Set K8S namespace for command"
  allNamespacesHelp = "Display objects from all namespaces"
  showLabelsHelp = "Show labels related to a Kubernetes object"
  outputYAMLHelp = "Display command output in YAML (if available)"
  outputJSONHelp = "Display command output in JSON (if available)"
  outputWideHelp = "Display wide command output (if available)"
  debugHelp = "Display the command line that is executed"
)

func init() {
  //sentry DSN setup
  raven.SetDSN("https://cc6610d18a9241e4b2f7cc645b86e258@sentry.io/1352406")

  cobra.OnInitialize()

  logging.SetFormatter(plainFormat)

  config := configuration.GetConfig()

  subcmd.PersistentFlags().BoolVarP(&config.Verbose, "verbose", "v", false, verboseHelp )
  subcmd.PersistentFlags().StringVarP(&config.Namespace, "namespace", "n", config.Namespace, namespaceHelp)
  subcmd.PersistentFlags().BoolVarP(&config.AllNamespaces, "all-namespaces", "A", false, allNamespacesHelp)
  subcmd.PersistentFlags().BoolVarP(&config.ShowLabels, "show-labels", "L", config.ShowLabels, showLabelsHelp)
  subcmd.PersistentFlags().BoolVarP(&config.OutputYAML, "yaml", "Y", config.OutputYAML, outputYAMLHelp)
  subcmd.PersistentFlags().BoolVarP(&config.OutputJSON, "json", "J", config.OutputJSON, outputJSONHelp)
  subcmd.PersistentFlags().BoolVarP(&config.OutputWide, "wide", "W", config.OutputWide, outputWideHelp)
  subcmd.PersistentFlags().BoolVarP(&config.Debug, "debug", "D", config.Debug, debugHelp)

  if flagPrettyLog {
    logging.SetFormatter(prettyFormat)
  }

  level, err := logging.LogLevel(flagLevel)
  if err != nil {
    fmt.Println("Invalid log level value. Falling back to debug")
    level = logging.DEBUG
  }
  logging.SetLevel(level, "k")

}


func main() {
  args := os.Args[1:]

  defer recoverPanic()

  if flagShowHelp || (len(args) > 0 && args[0] == "help") {
    flag.PrintDefaults()
    return
  }

  subcmd.Execute()
}

func recoverPanic() {
  raven.CapturePanicAndWait(func() {
    if rec := recover(); rec != nil {
      panic(rec)
    }
  }, map[string]string{
    "Version":      VERSION,
    "Platform":     runtime.GOOS,
    "Architecture": runtime.GOARCH,
    "goversion":    runtime.Version()})
}
