/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package configuration

import (
  "testing"
)

func TestConfigSingleton(t *testing.T) {
  orig := GetConfig()
  copy := GetConfig()

  if orig != copy {
    t.Errorf("Singleton has 2 addresses: %p and %p", orig, copy)
  }
}

func TestInitConfig(t *testing.T) {
  conf := initConfig()

  if conf.AllNamespaces == true {
    t.Error("config.AllNamespaces initialized to true")
  }

  if conf.ShowLabels == true {
    t.Error("config.ShowLabels initialized to true:")
  }

  if conf.OutputWide == true {
    t.Error("config.WideMode initialized to true")
  }

  if conf.Verbose == true {
    t.Error("config.Verbose initialized to true")
  }
  
}