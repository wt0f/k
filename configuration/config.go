/*

Copyright 2019 Gerard Hickey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package configuration

import (
  "strings"
  "fmt"
  "os"
  "sync"
  "runtime"

  //"github.com/op/go-logging"
  "github.com/getsentry/raven-go"
  "github.com/spf13/viper"
)

type TupleItem struct {
  Key   string
  Value string
}

type TupleArray []TupleItem


// Config stores application configurations
type config struct {
  AllNamespaces                     bool
  ShowLabels                        bool
  OutputWide                        bool
  OutputJSON                        bool
  OutputYAML                        bool
  Namespace                         string
  Verbose                           bool
  Debug                             bool
}

var runningConfig *config
var once sync.Once


func init() {
  if runningConfig == nil {
    once.Do(func() {
      runningConfig = initConfig()
    })
  }

  viper.SetConfigName("k") // name of config file (without extension)
  viper.AddConfigPath("$HOME/.kube")
  viper.AddConfigPath(".")

  raven.CapturePanic(func() {
    err := viper.ReadInConfig() // Find and read the config file
    if err != nil {
      if strings.Index(err.Error(), "Not Found") > 0 { // Handle errors reading the config file
        // Build the default config settings
        fmt.Println("Config file not found")
        err = SaveConfig()
        if err != nil {
          raven.CaptureErrorAndWait(err, nil)
          fmt.Printf("err = %s", err)
        }
      } else {
        panic(fmt.Errorf("Fatal error config file: %s \n", err))
      }
    }
    runningConfig.distillConfig()
  }, map[string]string{
    "Version":      "",
    "Platform":     runtime.GOOS,
    "Architecture": runtime.GOARCH,
    "goversion":    runtime.Version()})
}


func initConfig() *config {
  return &config{
    AllNamespaces:  false,
    ShowLabels:     false,
    OutputWide:     false,
    OutputJSON:     false,
    OutputYAML:     false,
    Verbose:        false,
  }
}

func GetConfig() *config {
  if runningConfig == nil {
    once.Do(func() {
      runningConfig = initConfig()
    })
  }
  return runningConfig
}


func SaveConfig() error {
  return runningConfig.saveConfig()
}

func (c *config) saveConfig() error {
  raven.CapturePanic(func() {
    // err := 
    c.populateViper()
    // if err != nil {
    //   fmt.Println("Problem populating Viper config")
    //   return err
    // }
  }, map[string]string{
    "Version":      "",
    "Platform":     runtime.GOOS,
    "Architecture": runtime.GOARCH,
    "goversion":    runtime.Version()})

  configPath := os.Getenv("HOME") + "/.kube/k.yaml"
  return viper.WriteConfigAs(configPath)
}


func (c *config) populateViper() error {
  viper.Set("showLabels", c.ShowLabels)
  viper.Set("OutputWide", c.OutputWide)

  return nil
}

func (c *config) distillConfig() error {
  c.ShowLabels = viper.GetBool("showLabels")
  c.OutputWide = viper.GetBool("OutputWide")

  return nil
}